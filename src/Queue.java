public class Queue {
    private Node head;
    private Node tail;
    private int size = 0;

    public Node dequeue(){
        Node removed = head;
        Node newHead = head.getNext();
        head.setNext(null);
        head = newHead;
        size--;
        return removed;
    }
    public void enqueue(Node node){
        if(head == null){
            head = node;
        }
        else{
            tail.setNext(node);
        }
        size++;
        tail = node;
    }
    public int getSize(){return size;}
}
